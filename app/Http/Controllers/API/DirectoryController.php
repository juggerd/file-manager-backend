<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class DirectoryController extends BaseController
{
//    public function test(Request $request) {
//        return response()->json(['items'=> []]);
//    }

    public function getDirectory(Request $request) {
        $directory = $request->get('directory', '/');
        $result = [];
        foreach (scandir($directory) as $item) {
            if (in_array($item, ['.', '..'])) {
                continue;
            }
            $flag = 'f';
            if (is_dir($directory.'/'.$item)) $flag = 'd';
            if (is_link($directory.'/'.$item)) $flag = 'l';
            $result[] = [
                'name' => $item,
                'directory' => $directory,
                'flag' => $flag
            ];
        }
        return response()->json($result);
    }
}
